## This is a file which creates a Virtual machine with vagrant

Here is some text.

To build, start, restart and remove the vagrant vms, respectively run these commands in the terminal.

vagrant init (initialises a Vagrant Vm)
vagrant up (starts a vagrant environment)
vagrant ssh (allows one to connect to the virtual machine)
vagrant reload (restarts "..")
vagrant cleanup (destroys "..")
